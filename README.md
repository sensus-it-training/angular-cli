# Implementacion y desarrollo [Instructivo]
## Requerimientos de desarrollo (Software instalado)
* Sistema Operativo Linux
* Sublime o VS Code (IDE de desarrollo)
* Git 
* Docker 
* Docker compose 
* Certificado generado en la computadora y copiado en GitLab

## Modificaciones del archivo README.md
- Target branch: actualizacion_readme

# Correr proyectos con docker
## Iniciar por primera vez el proyecto
* Las imagenes de los contenedores de cada proyecto se encuentran en la carpeta raiz de cada proyecto. Esto quiere decir que para correr el entorno de desarrollo de cada proyecto se tiene que ingresar al directorio clonado de GitLab y correr el comando "docker-compose up -d", en el directorio raiz. El parametro up indica que tiene que levantar el servicio especificado en el archivo docker-compose.yml y la opcion -d que va a correr en background. La primera vez que se corra ese proyecto, sera necesario construir la imagen, esto ultimo lo realiza automaticamente, dependiendo de la conexion y la computadora puede llegar a tardar 30 minutos  
## Trabajar con los contenedores ya construidos
* Una vez que tenemos el contenedor del proycto corriendo en nuestro sistema:
- docker ps (Podemos ver los contenedores corriendo)
- docker exec -it [nombre-del-contenedor] bash (podemos ingresar al contenedor para trabajar con Angular CLI. Lo que hace este comando es ejecutar una consola interactiva {exec -it} con bash en el contenedor [nombre-del-contenedor])
- exit. comando para salir del contenedor. Una vez que se terima de trabajar con Angular CLI dentro del contenedor podemos volver a nuestro equipo con exit. Esto no va a detener la ejecucion del contenedor, solamente desconectarnos.

# Trabajar con Angular CLI
- Angular solamente va a estar disponible dentro de los contenedores de cada proyecto. De esta forma nos aseguramos que todos estamos trabajando con las mismas imagenes y versiones de todo el ecosistema para el front end. Lo mismo aplica pra el backend.
- Primero debemos ingresar dentro del contenedor, Ver el punto anterior [Correr proyectos con docker].
- Importante: Todos los componentes, modulos, interfaces, servicios y demas van a crearse dentro de la categoria/directorio padre a la que correspondan. Esto quiere decir, por ejemplo, que si genero una interfaz para usuarios, dicho archivo va a crearse de la siguiente manera: ng g i usuarios/usuario (generando el archivo dentro de la carpeta usuarios la cual contiene todo lo relacionado a usuarios)
* ng generate component [nombre-componente] (Genera un componente. De no especificarse directorio, lo crea en el directorio app. para especificar directorio se debe anteponer [nombre-directorio/], siguiendo el mismo modelo para subdirectorios)
* ng generate module [nombre-modulo] (Genera un modulo. De no especificarse directorio, lo crea en el directorio app. para especificar directorio se debe anteponer [nombre-directorio/], siguiendo el mismo modelo para subdirectorios)
* ng generate interface (genera una interfaz. De no especificarse directorio, lo crea en el directorio app. para especificar directorio se debe anteponer [nombre-directorio/], siguiendo el mismo modelo para subdirectorios)
* ng generate service [nombre-servicio] (Genera un servicio. De no especificarse directorio, lo crea en el directorio app. para especificar directorio se debe anteponer [nombre-directorio/], siguiendo el mismo modelo para subdirectorios)


**# Faltan los comandos de GIT!!!!! **

**Faltan los comandos de consola**
